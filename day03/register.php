<html>
<head>
    <meta charset="UTF-8">
	<style>
		body {
		   margin:0px;
		   padding: 0px;
		}
		
		.form-content{
			width:400px;
			height: 300px;
			display: flex;
			flex-wrap: wrap;
			align-content: center;
			margin: 0 auto;
			padding:20px;
			outline: dashed 1px black;
			
		}
		
		form{
			width: 100%;
		}
		
		
		
		.form-action{
			display: flex;
			justify-content: center;
		}
		
		.form-row{
			margin: 16px auto;
			width:100%;
			height: 40px;
			align-items: center;
			display: flex;
		
		}
		
		.title-row{
			padding : 8px 10px 0px;
			border-style: solid;
			border-width: 2px;
			border-color: #4679a8;
			display: block;
			text-align: center;
			line-height: 150%;
			font-size: 1.2em;
			background-color:#5b9bd5;
			color:#fff;
			width:84px;
		}
		
		.input-row{
			height:100%;
			width:100%;
			margin-left: 20px;
			
		}
		
		.input{
			height:100%;
		}
 
		.border{
			border-style: solid;
			border-width: 2px;
			border-color: #4679a8;
		}
		
		.w-100{
			width:100%;
		}
		
		.d-flex{
			display:flex;
		}
		
		.flex-shink-0{
			flex-shrink: 0!important;
		}
		
		.flex-wrap {
			flex-wrap: wrap!important;
		}
		
		.p-2 {
			padding: 0.5rem!important;
		}
		
		span{
			position: absolute;
			bottom: 0;
		}
		
		.ml-20px{
			margin-left:20px;
		}
		
		.mr-2{
			margin-right:0.5em;
		}
		
		.p-relative{
			position: relative;
		}
		
		.mt-10{
			margin-top: 2.5em;
		}
		
		.btn{
			padding: 12px 32px;
			border-style: solid;
			border-width: 2px;
			border-color: #4679a8;
			border-radius:8px;
			background-color:#70ad47;
			color:#fff;
			font-size: 16px;
		}
		
		.ai-center{
			align-items: center;
		}
	</style>
</head>
<body>
	<div class="form-content">
		<form action="welcome.php" method="post">
			<div class="form-row">
				<div class="title-row flex-shink-0">Họ và tên</div>
				<div class="input-row"><input class="w-100 input border" type="text" /></div>
				
			</div>
			<div class="form-row">
				<div class="title-row flex-shink-0">Giới tính</div>
				<div class="input-row d-flex">
					<?php
						$genders = array("Nam", "Nữ");
						
						for ($x = 0; $x < count($genders); $x++) {
						  echo "<label class='d-flex ai-center'>
										<input class='border' type='radio' name='gender' value='$x'>
										$genders[$x]
									</label>";
						}
					?>
				</div>
			</div>
			<div class="form-row">
				<div class="title-row flex-shink-0">Phân khoa</div>
				<div  class="input-row">
					<select class="input border">
						<?php
							$majors = array("0"=>"--Chọn phân khoa--" ,"MAT"=>"Khoa học máy tính", "KDL"=>"Khoa học vật liệu");
							
							foreach($majors as $value => $text) {
								echo "<option value='$value'>$text</option>";
							}
						?>
					</select>
				</div>
			</div>
			<div class="form-action mt-10"><input class="btn" type="submit" value="Đăng ký" /></div>
		</form>
	</div>
</body>
</html>