<?php
	include 'database.php';
	
	if ($_SERVER["REQUEST_METHOD"] == "GET") {
		$keyword = trim($_GET["keyword"], " \n\r\t\v\x00");
		$major =  trim($_GET["major"], " \n\r\t\v\x00");
		
		$req = "select id, full_name, major from students where full_name like '%$keyword%'";
		if($major != ''){
			$req = $req . " and major = '$major'";
		}
		
		$resp = array("success"=>FALSE);
		try{
			$result = $conn->query($req);
			
			$resp["data"] = array();
			if ($result->num_rows > 0) {
			  while($row = $result->fetch_assoc()) {
				$_major = "";
				if($row["major"] === "MAT"){
					$_major = "Khoa học máy tính";
				}else if($row["major"] === "KDL"){
				  $_major = "Khoa học vật liệu";
				}
				
				$student = array(
					"id"=>$row["id"],
					"full_name"=>$row["full_name"],
					"major"=>$_major
				);
				array_push($resp["data"],$student);
			  }
			  $resp["success"] = TRUE;
			  echo json_encode($resp, JSON_UNESCAPED_UNICODE);
			}
		} catch(Exception $e){
			$resp["success"] = FALSE;
			echo json_encode($resp, JSON_UNESCAPED_UNICODE);
		}
		
		$conn->close();
	}
	
?>