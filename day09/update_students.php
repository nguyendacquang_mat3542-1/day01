<html>
<head>
    <meta charset="UTF-8">
	<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
	<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous"></script>
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.10.0/css/bootstrap-datepicker.min.css" integrity="sha512-34s5cpvaNG3BknEWSuOncX28vz97bRI59UnVtEEpFX536A7BtZSJHsDyFoCl8S7Dt2TPzcrCEoHBGeM4SUBDBw==" crossorigin="anonymous" referrerpolicy="no-referrer" />
</head>
<style>
	body {
	   margin:40px 0px;
	   padding: 0px;
	}
	
	.input-title{
		padding : 8px 10px 0px;
		display: block;
		text-align: center;
		line-height: 150%;
		font-size: 1em;
		background-color:#70ad47;
		color:#fff;
		position: relative;
	}
	
	.validation::after{
		content: "*";
		color: red;
		position: absolute;
		right: 6px;
		top: 4;
		font-size:1.2em;
	}
	
	.custom-border{
		border-style: solid;
		border-width: 2px;
		border-color: #4679a8;
	}
	
	
	
	input[type="radio"]{
		appearance: none;
		width: 1em;
		height: 1em;
		background-color: #fff;
		background-repeat: no-repeat;
		background-position: center;
		background-size: contain;
		border: 2px solid #4679a8;
		border-radius: 50%;
	}
	
	input[type="radio"]:checked {
	  background-color: #70ad47; 
	  
	}
	
	input[type="radio"]:hover{
		cursor:pointer;
	}
	
	.btn-register{
		color:#fff;
		background-color: #70ad47;
		border-radius: 10px;
		border: 2px solid #4679a8;
		padding: 8px 40px;
	}
	
	.font-size-cus{
		font-size:0.9em;
	}
	
	.danger{
		color:red;
	}
	
	.h-fit-content{
		height: fit-content;
	}
</style>
<body>
	<?php
		
		$full_name = '';
		$gender = '';
		$major = '';
		$birthday = '';
		$address = '';
		$url_image = '';
		try{
			include 'database.php';
			$uri = $_SERVER["REQUEST_URI"];
			$uriArray = explode('/', $uri);
			$id = $uriArray[2];
			$query = "select * from students where id=$id";
			
			$rs = $conn->query($query);
			if ($rs->num_rows > 0){
				$data = $rs->fetch_assoc();
				$id = $data["id"];
				$full_name = $data["full_name"];
				$gender = $data["gender"];
				$major = $data["major"];
				$birthday = $data["birthday"];
				$address = $data["address"];
				$url_image = $data["image"];
			}
		}catch(Exception $e){
			
		}
	?>
	<div id="alert" class="d-flex flex-column"></div>
	<form enctype="multipart/form-data">
		<div class="container">
			<div class="row align-items-center mt-3">
				<div class="d-flex justify-content-center gap-3">
					<div class="col-1"><label class="input-title custom-border validation" for="full_name">Họ và tên</label></div>
					<div class="col-3">
					<?php 
						echo "<input class='w-100 h-100 custom-border font-size-cus' type='text' id='full_name' name='full_name' value='$full_name'/>";
					?>
					</div>
				</div>
				
			</div>
			<div class="row align-items-center mt-3">
				<div class="d-flex justify-content-center gap-3">
					<div class="col-1"><label class="input-title custom-border validation">Giới tính</label></div>
					<div class="col-3 d-flex gap-3 ps-2">
						<?php
							$genders = array("Nam", "Nữ");
							$checked;
							for ($x = 0; $x < count($genders); $x++) {
								$checked = '';
								if($x == $gender){
									$checked = 'checked';
								}
								echo "<div class='radio-item d-flex gap-2'>
											<input class='align-self-center' id='$x' type='radio' name='gender' value='$x' $checked>
											<label class='align-self-center' for='$x'>$genders[$x]</label>
										</div>";
							}
						?>
					</div>
				</div>
			</div>
			<div class="row align-items-center mt-3">
				<div class="d-flex justify-content-center gap-3">
					<div class="col-1"><label class="input-title custom-border validation" for="major">Phân khoa</label></div>
					<div  class="col-3">
						<select class="w-50 h-100 custom-border font-size-cus" id="major" name="major">
							<?php
								$majors = array(""=>"--Chọn phân khoa--" ,"MAT"=>"Khoa học máy tính", "KDL"=>"Khoa học vật liệu");
								
								$selected;
								foreach($majors as $value => $text) {
									$selected = '';
									if($value == $major){
										$selected = 'selected';
									}
									echo "<option value='$value' $selected>$text</option>";
								}
							?>
						</select>
					</div>
				</div>
			</div>
			<div class="row align-items-center mt-3">
				<div class="d-flex justify-content-center gap-3">
					<div class="col-1"><label class="input-title custom-border validation" for="birthday">Ngày sinh</label></div>
					<div  class="col-3 date-picker">
					<?php
						$date = DateTime::createFromFormat('Y-m-d H:i:s', $birthday);
						$birthday = $date->format('d/m/Y');
						echo "<input class='w-50 h-100 custom-border font-size-cus' type='text' id='birthday' name='birthday' value='$birthday'/>";
					?>
						
					</div>
				</div>
			</div>
			<div class="row mt-3 h-fit-content">
				<div class="d-flex justify-content-center gap-3">
					<div class="col-1"><label class="input-title custom-border" for="address">Địa chỉ</label></div>
					<div  class="col-3 h-300">
						<?php
							echo "<textarea id='address' class='w-100 h-100 custom-border font-size-cus' name='address' cols='40' rows='4' style='resize: none;'>$address</textarea>";
						?>
					</div>
				</div>
			</div>
			<div class="row mt-3 h-fit-content">
				<div class="d-flex justify-content-center gap-3">
					<div class="col-1"><label class="input-title custom-border" for="image">Hình ảnh</label></div>
					<div  class="col-3 h-300 align-self-center">
						<input class="mb-2" type="file" id="image" accept="image/*" name="image"/>
						<?php
							echo "<img class='w-50' id='img' src='$url_image' />";
						?>
					</div>
					
				</div>
			</div>
			<div class="row mt-3">
				<div class="d-flex justify-content-center">
					<?php echo "<input type='hidden' value='$id' name='id'/>"; ?>
					<input class="btn btn-register" type="button" value="Xác nhận" />
				</div>
			</div>
		</div>
	</form>
	
	<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.7.1/jquery.min.js" integrity="sha512-v2CJ7UaYy4JwqLDIrZUI/4hqeoQieOmAZNXBeQyjo21dadnwR+8ZaIJVT8EE2iyI61OV8e6M8PP2/4hpQINQ/g==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.10.0/js/bootstrap-datepicker.min.js" integrity="sha512-LsnSViqQyaXpD4mBBdRYeP6sRwJiJveh2ZIbW41EBrNmKxgr/LFZIiWT6yr+nycvhvauz8c2nYMhrP80YhG7Cw==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
	<script type="text/javascript">
		$(document).ready(() => {
			const dateRegex = /^(?=\d)(?:(?:31(?!.(?:0?[2469]|11))|(?:30|29)(?!.0?2)|29(?=.0?2.(?:(?:(?:1[6-9]|[2-9]\d)?(?:0[48]|[2468][048]|[13579][26])|(?:(?:16|[2468][048]|[3579][26])00)))(?:\x20|$))|(?:2[0-8]|1\d|0?[1-9]))([-.\/])(?:1[012]|0?[1-9])\1(?:1[6-9]|[2-9]\d)?\d\d(?:(?=\x20\d)\x20|$))?(((0?[1-9]|1[012])(:[0-5]\d){0,2}(\x20[AP]M))|([01]\d|2[0-3])(:[0-5]\d){1,2})?$/;
			
			$('.date-picker input').datepicker({format: 'dd/mm/yyyy'})
			
			$("input[type='button']").on("click", function(e) {
			
				//e.preventDefault();
				
				$('#alert div').remove()
				
				const full_name = $('#full_name').val().trim()
				const gender = $('input[name=gender]:checked').val()
				const major = $('#major').find('option:selected').val()
				const birthday = $('#birthday').val()
				
				var isValid = true
				if(!full_name){
					const msg = 'Hãy nhập tên.'
					$('#alert').append(gen_alert_html(msg))
					isValid = false
				}
				
				if(!gender){
					const msg = 'Hãy chọn giới tính.'
					$('#alert').append(gen_alert_html(msg))
					isValid = false
				}
				
				if(!major){
					const msg = 'Hãy chọn phân khoa.'
					$('#alert').append(gen_alert_html(msg))
					isValid = false
				}
				
				
				if(!birthday){
					const msg = 'Hãy nhập ngày sinh.'
					$('#alert').append(gen_alert_html(msg))
					isValid = false
				}
				
				if(birthday && !dateRegex.test(birthday)){
					const msg = 'Hãy nhập ngày sinh đúng định dạng.'
					$('#alert').append(gen_alert_html(msg))
					isValid = false
				}
				
				
				if(isValid) {
					const data = $('form').serialize();
					console.log(data)
					$.ajax({
						type: "POST",
						url: "/save_info.php",
						data: data,
						success: function (resp) {
							console.log(resp)
							window.location.href = '/list_students.php';
						},
						error: function () {
							alert("Có lỗi khi lưu!")
						},
					});
				}
				
			});
			
			$('#image').change(function(e){
			  console.log($('#image').val())
			  readURL(e);
			});
		})
		
		function readURL(e) {
		  
			var reader = new FileReader();
			var file = e.originalEvent.srcElement.files[0];
			reader.onload = function (e) {
			  $('img').attr('src', e.target.result);
			};

			reader.readAsDataURL(file);
		  
		}
		
		function gen_alert_html(msg){
			return `<div style="padding-left:40%;"><span class='danger'>${msg}</span></div>`
		}
		
	</script>
</body>
</html>