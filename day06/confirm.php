<html>
<head>
    <meta charset="UTF-8">
	<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
	<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous"></script>
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.10.0/css/bootstrap-datepicker.min.css" integrity="sha512-34s5cpvaNG3BknEWSuOncX28vz97bRI59UnVtEEpFX536A7BtZSJHsDyFoCl8S7Dt2TPzcrCEoHBGeM4SUBDBw==" crossorigin="anonymous" referrerpolicy="no-referrer" />
</head>
<style>
	body {
	   margin:40px 0px;
	   padding: 0px;
	}
	
	.input-title{
		padding : 8px 10px 0px;
		display: block;
		text-align: center;
		line-height: 150%;
		font-size: 1em;
		background-color:#70ad47;
		color:#fff;
		position: relative;
	}
	
	.validation::after{
		content: "*";
		color: red;
		position: absolute;
		right: 6px;
		top: 4;
		font-size:1.2em;
	}
	
	.custom-border{
		border-style: solid;
		border-width: 2px;
		border-color: #4679a8;
	}
	
	
	
	input[type="radio"]{
		appearance: none;
		width: 1em;
		height: 1em;
		background-color: #fff;
		background-repeat: no-repeat;
		background-position: center;
		background-size: contain;
		border: 2px solid #4679a8;
		border-radius: 50%;
	}
	
	input[type="radio"]:checked {
	  background-color: #70ad47; 
	  
	}
	
	input[type="radio"]:hover{
		cursor:pointer;
	}
	
	.btn-register{
		color:#fff;
		background-color: #70ad47;
		border-radius: 10px;
		border: 2px solid #4679a8;
		padding: 8px 40px;
	}
	
	.font-size-cus{
		font-size:0.9em;
	}
	
	.danger{
		color:red;
	}
	
	.h-fit-content{
		height: fit-content;
	}
</style>

<?php 
if ($_SERVER['REQUEST_METHOD'] !== 'POST') 
  {
      die;
  }

  if (!isset($_FILES["image"])) 
  {
      die;
  }

  if ($_FILES["image"]['error'] != 0)
  {
    
    die;
  }

  $target_dir    = "uploads/";
  $target_file   = $target_dir . basename($_FILES["image"]["name"]);
  $allowUpload   = true;
  $allowtypes    = array('jpg', 'png', 'jpeg', 'gif');
  $imageFileType = pathinfo($target_file,PATHINFO_EXTENSION);


  if(isset($_POST["submit"])) {
      //Kiểm tra xem có phải là ảnh bằng hàm getimagesize
      $check = getimagesize($_FILES["image"]["tmp_name"]);
      if($check !== false) 
      {
          $allowUpload = true;
      } 
      else 
      {
          $allowUpload = false;
      }
  }
  
  if (file_exists($target_file)) 
  {
      $allowUpload = false;
  }


  if ($allowUpload) 
  {
      
      if (!move_uploaded_file($_FILES["image"]["tmp_name"], $target_file))
      {
          echo "Có lỗi xảy ra khi upload file.";
      }
      
  }
?>

<body>
	<div id="alert" class="d-flex flex-column"></div>
	<form id="form-confirm">
		<div class="container">
			<div class="row align-items-center mt-3">
				<div class="d-flex justify-content-center gap-3">
					<div class="col-1"><label class="input-title custom-border">Họ và tên</label></div>
					<div class="col-3 d-flex align-self-center"><?php 
						echo $_POST["full_name"]; 
						$full_name = $_POST["full_name"]; 
						echo "<input type='hidden' value='$full_name' name='full_name' />";
					?></div>
				</div>
				
			</div>
			<div class="row align-items-center mt-3">
				<div class="d-flex justify-content-center gap-3">
					<div class="col-1"><label class="input-title custom-border" >Giới tính</label></div>
					<div class="col-3 d-flex align-self-center">
						<?php 
							$genders = array("Nam", "Nữ");
							$gender_index = $_POST["gender"];
							echo "$genders[$gender_index]";
							echo "<input type='hidden' value='$gender_index' name='gender'/>";
						?>
					</div>
					
				</div>
			</div>
			<div class="row align-items-center mt-3">
				<div class="d-flex justify-content-center gap-3">
					<div class="col-1"><label class="input-title custom-border" >Phân khoa</label></div>
					<div class="col-3 d-flex align-self-center">
						<?php 
							$majors = array(""=>"--Chọn phân khoa--" ,"MAT"=>"Khoa học máy tính", "KDL"=>"Khoa học vật liệu");
							$major_key = $_POST["major"];
							$major_value = '';
							if($major_key!= ""){
								echo "$majors[$major_key]";
							}
							
							echo "<input type='hidden' value='$major_key' name='major' />";
						?>
					</div>
				</div>
			</div>
			<div class="row align-items-center mt-3">
				<div class="d-flex justify-content-center gap-3">
					<div class="col-1"><label class="input-title custom-border" >Ngày sinh</label></div>
					<div  class="col-3 d-flex align-self-center">
						<?php 
							echo $_POST["birthday"]; 
							$birthday = $_POST["birthday"];
							echo "<input type='hidden' value='$birthday' name='birthday'/>";
						?>
					</div>
				</div>
			</div>
			<div class="row mt-3 h-fit-content">
				<div class="d-flex justify-content-center gap-3">
					<div class="col-1"><label class="input-title custom-border">Địa chỉ</label></div>
					<div  class="col-3 d-flex align-self-center">
						<?php 
							echo $_POST["address"]; 
							$address = $_POST["address"]; 
							echo "<input type='hidden' value='$address' name='address'/>";
						?>
					</div>
				</div>
			</div>
			<div class="row mt-3 h-fit-content">
				<div class="d-flex justify-content-center gap-3">
					<div class="col-1"><label class="input-title custom-border">Hình ảnh</label></div>
					<div  class="col-3">
						<?php 
							echo "<img class='w-50' src='$target_file'>";
							echo "<input type='hidden' value='$target_file' name='image'/>";
						?>
					</div>
				</div>
			</div>
			<div class="row mt-3">
				<div class="d-flex justify-content-center">
					<input class="btn btn-register" type="submit" value="Xác nhận" />
				</div>
			</div>
		</div>
	</form>
	
	
	<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.7.1/jquery.min.js" integrity="sha512-v2CJ7UaYy4JwqLDIrZUI/4hqeoQieOmAZNXBeQyjo21dadnwR+8ZaIJVT8EE2iyI61OV8e6M8PP2/4hpQINQ/g==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
	<script type="text/javascript">
		$(document).ready(() => {
			$('#form-confirm').submit((e) =>{
				e.preventDefault();
				const formData = $('#form-confirm').serialize(); 
				console.log(formData)
				$.ajax({
					type: "POST",
					url: "save_info.php",
					data: formData,
					success: function (data) {
						console.log(data)
						alert("Lưu thành công!")
					},
					error: function () {
						alert("Có lỗi khi lưu!")
					},
				});
			});
		})
	</script>
</body>
</html>