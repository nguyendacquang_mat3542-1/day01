<html>
<head>
    <meta charset="UTF-8">
</head>
<body>
	<label><?php
		date_default_timezone_set('Asia/Ho_Chi_Minh');
        $hour_minutes = date('G:i');
        $day_of_week;
        if(((int)date('N',time())) < 7){
            $day_of_week = 'thứ '.((int)date('N')) + 1;
        }else{
            $day_of_week = 'Chủ nhật';
        }
        $date = date('d/m/Y');
        echo 'Bây giờ là ' . $hour_minutes . ', '.$day_of_week.' ngày '.$date.'';
        ?></label>
    <form action="#" method="post">
        <label for="username">Tên đăng nhập</label>
        <input type="text" name="username"/><br>
        <label for="pwd">Mật khẩu</label>
        <input type="password" name="pwd"/><br>
    	<input type="submit" value="Đăng nhập">
    </form>
</body>
</html>