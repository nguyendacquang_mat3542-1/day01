<html>
<head>
    <meta charset="UTF-8">
	<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
	<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous"></script>
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.10.0/css/bootstrap-datepicker.min.css" integrity="sha512-34s5cpvaNG3BknEWSuOncX28vz97bRI59UnVtEEpFX536A7BtZSJHsDyFoCl8S7Dt2TPzcrCEoHBGeM4SUBDBw==" crossorigin="anonymous" referrerpolicy="no-referrer" />
</head>
<style>
	body {
	   margin:40px 0px;
	   padding: 0px;
	}
	
	.input-title{
		padding : 8px 10px 0px;
		display: block;
		text-align: center;
		line-height: 150%;
		font-size: 1em;
		background-color:#70ad47;
		color:#fff;
		position: relative;
	}
	
	.validation::after{
		content: "*";
		color: red;
		position: absolute;
		right: 6px;
		top: 4;
		font-size:1.2em;
	}
	
	.custom-border{
		border-style: solid;
		border-width: 2px;
		border-color: #4679a8;
	}
	
	
	
	input[type="radio"]{
		appearance: none;
		width: 1em;
		height: 1em;
		background-color: #fff;
		background-repeat: no-repeat;
		background-position: center;
		background-size: contain;
		border: 2px solid #4679a8;
		border-radius: 50%;
	}
	
	input[type="radio"]:checked {
	  background-color: #70ad47; 
	  
	}
	
	input[type="radio"]:hover{
		cursor:pointer;
	}
	
	.btn-register{
		color:#fff;
		background-color: #70ad47;
		border-radius: 10px;
		border: 2px solid #4679a8;
		padding: 8px 40px;
	}
	
	.font-size-cus{
		font-size:0.9em;
	}
	
	.danger{
		color:red;
	}
	
	.h-fit-content{
		height: fit-content;
	}
</style>
<body>
	<div id="alert" class="d-flex flex-column"></div>
	<form action="regist_student.php" method="POST">
		<div class="container">
			<div class="row align-items-center mt-3">
				<div class="d-flex justify-content-center gap-3">
					<div class="col-2"><label class="input-title custom-border validation" for="full_name">Họ và tên</label></div>
					<div class="col-3"><input class="w-100 h-100 custom-border font-size-cus" type="text" id="full_name" name="full_name"/></div>
				</div>
				
			</div>
			<div class="row align-items-center mt-3">
				<div class="d-flex justify-content-center gap-3">
					<div class="col-2"><label class="input-title custom-border validation">Giới tính</label></div>
					<div class="col-3 d-flex gap-3 ps-2">
						<?php
							$genders = array("Nam", "Nữ");
							
							for ($x = 0; $x < count($genders); $x++) {
							  echo "<div class='radio-item d-flex gap-2'><input class='align-self-center' id='$x' type='radio' name='gender' value='$x'> <label class='align-self-center' for='$x'>$genders[$x]</label></div>";
							}
						?>
					</div>
				</div>
			</div>
			<div class="row align-items-center mt-3">
				<div class="d-flex justify-content-center gap-3">
					<div class="col-2"><label class="input-title custom-border validation" for="birthday">Ngày sinh</label></div>
					<div  class="col-3">
						
							
						<select class="w-30 h-100 custom-border font-size-cus" id="year">
							<?php
								echo "<option value=''>Năm</option>";
								for ($x = (int)date("Y") - 40; $x <= (int)date("Y") - 15; $x++) {
								  echo "<option value='$x'>$x</option>";
								}
							?>
						</select>
						
						
						<select class="w-30 h-100 custom-border font-size-cus" id="month">
							<?php
								echo "<option value=''>Tháng</option>";
								for ($x = 1; $x <= 12; $x++) {
								  echo "<option value='$x'>$x</option>";
								}
							?>
						</select>
						<select class="w-30 h-100 custom-border font-size-cus" id="day">
							<?php
								echo "<option value=''>Ngày</option>";
								for ($x = 1; $x <= 31; $x++) {
								  echo "<option value='$x'>$x</option>";
								}
							?>
						</select>
						
						<input id="birthday" name="birthday" type="hidden"/>
						
						
					</div>
				</div>
			</div>
			
			<div class="row align-items-center mt-3">
				<div class="d-flex justify-content-center gap-3">
					<div class="col-2"><label class="input-title custom-border validation" for="address">Địa chỉ</label></div>
					<div  class="col-3">
						
							
						<select class="w-30 h-100 custom-border font-size-cus" id="city">
							<?php
								 $cities = array(
									'' => 'Thành phố',
									'HN' => 'Hà Nội',
									'HCM' => 'Hồ Chí Minh'
								);
								 foreach ($cities as $key => $value) {
									echo "<option value='$key'>$value</option>";
								}
								
							?>
						</select>
						
						
						<select class="w-30 h-100 custom-border font-size-cus" id="district">
							<option value="">Quận</option>
						</select>
						<input id="address" name="address" type="hidden"/>
						
					
	
					</div>
				</div>
			</div>
			
			<div class="row mt-3 h-fit-content">
				<div class="d-flex justify-content-center gap-3">
					<div class="col-2"><label class="input-title custom-border" for="info">Thông tin khác</label></div>
					<div  class="col-3 h-300">
						<textarea id="info" class="w-100 h-100 custom-border font-size-cus" name="info" cols="40" rows="4" style="resize: none;"></textarea>
						
					</div>
				</div>
			</div>
			
			<div class="row mt-3">
				<div class="d-flex justify-content-center">
					<input id="btn-submit" class="btn btn-register" type="button" value="Đăng ký" />
				</div>
			</div>
		</div>
	</form>
	
	<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.7.1/jquery.min.js" integrity="sha512-v2CJ7UaYy4JwqLDIrZUI/4hqeoQieOmAZNXBeQyjo21dadnwR+8ZaIJVT8EE2iyI61OV8e6M8PP2/4hpQINQ/g==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.10.0/js/bootstrap-datepicker.min.js" integrity="sha512-LsnSViqQyaXpD4mBBdRYeP6sRwJiJveh2ZIbW41EBrNmKxgr/LFZIiWT6yr+nycvhvauz8c2nYMhrP80YhG7Cw==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
	<script type="text/javascript">
		$(document).ready(() => {
			const cities = {HN:"Hà Nội", HCM:"Hồ Chí Minh"}
			var districts = {}
			$("#city").change(function() {
				$('#district').empty();
				
			  if ($(this).val() == 'HN') {
					districts = {
						HM:'Hoàng Mai',
						TT:'Thanh Trì',
						NTL:'Nam Từ Liêm',
						HD:'Hà Đông',
						CG:'Cầu giấy'
					}
					
					
			  } else if ($(this).val() == 'HCM') {

					districts = {
						Q1:'Quận 1',
						Q2:'Quận 2',
						Q3:'Quận 3',
						Q7:'Quận 7',
						Q9:'Quận 9'
					}
					

			  }
				  Object.keys(districts).forEach(function(key) {					  
							$('#district').append(`<option value='${key}'>${districts[key]}</option>`)
						});

		  })
			
			$("#btn-submit").on("click", function(e) {
				
				
				$('#alert div').remove()
				
				const full_name = $('#full_name').val().trim()
				const gender = $('input[name=gender]:checked').val()
				
				const year = $('#year').val()
				var month = $('#month').val()
				var day = $('#day').val()
				
				const city = $('#city').val()
				const district = $('#district').val()
				
				var isValid = true
				if(!full_name){
					const msg = 'Hãy nhập tên.'
					$('#alert').append(gen_alert_html(msg))
					isValid = false
				}
				
				if(!gender){
					const msg = 'Hãy chọn giới tính.'
					$('#alert').append(gen_alert_html(msg))
					isValid = false
				}
				
				if(!year || !month || !day){
					const msg = 'Hãy chọn ngày sinh.'
					$('#alert').append(gen_alert_html(msg))
					isValid = false
				}
				
				if(!city || !district){
					const msg = 'Hãy chọn địa chỉ.'
					$('#alert').append(gen_alert_html(msg))
					isValid = false
				}
				
				
				if(isValid) {
					if(day < 10){
						day=`0${day}`
					}
					if(month < 10){
						month=`0${month}`
					}
					$('#birthday').val(`${day}/${month}/${year}`)
					$('#address').val(`${districts[district]}-${cities[city]}`)
					$('form').trigger('submit')
				}
				
			});
		})
		
		function gen_alert_html(msg){
			return `<div style="padding-left:40%;"><span class='danger'>${msg}</span></div>`
		}
	</script>
</body>
</html>