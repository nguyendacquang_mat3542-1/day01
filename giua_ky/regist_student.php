<html>
<head>
    <meta charset="UTF-8">
	<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
	<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous"></script>
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.10.0/css/bootstrap-datepicker.min.css" integrity="sha512-34s5cpvaNG3BknEWSuOncX28vz97bRI59UnVtEEpFX536A7BtZSJHsDyFoCl8S7Dt2TPzcrCEoHBGeM4SUBDBw==" crossorigin="anonymous" referrerpolicy="no-referrer" />
</head>
<style>
	body {
	   margin:40px 0px;
	   padding: 0px;
	}
	
	.input-title{
		padding : 8px 10px 0px;
		display: block;
		text-align: center;
		line-height: 150%;
		font-size: 1em;
		background-color:#70ad47;
		color:#fff;
		position: relative;
	}
	
	.validation::after{
		content: "*";
		color: red;
		position: absolute;
		right: 6px;
		top: 4;
		font-size:1.2em;
	}
	
	.custom-border{
		border-style: solid;
		border-width: 2px;
		border-color: #4679a8;
	}
	
	
	
	input[type="radio"]{
		appearance: none;
		width: 1em;
		height: 1em;
		background-color: #fff;
		background-repeat: no-repeat;
		background-position: center;
		background-size: contain;
		border: 2px solid #4679a8;
		border-radius: 50%;
	}
	
	input[type="radio"]:checked {
	  background-color: #70ad47; 
	  
	}
	
	input[type="radio"]:hover{
		cursor:pointer;
	}
	
	.btn-register{
		color:#fff;
		background-color: #70ad47;
		border-radius: 10px;
		border: 2px solid #4679a8;
		padding: 8px 40px;
	}
	
	.font-size-cus{
		font-size:0.9em;
	}
	
	.danger{
		color:red;
	}
	
	.h-fit-content{
		height: fit-content;
	}
</style>

<?php 
if ($_SERVER['REQUEST_METHOD'] !== 'POST') 
{
  die;
}

?>

<body>
	<div id="alert" class="d-flex flex-column"></div>
	<form id="form-confirm">
		<div class="container">
			<div class="row align-items-center mt-3">
				<div class="d-flex justify-content-center gap-3">
					<div class="col-1"><label class="input-title custom-border">Họ và tên</label></div>
					<div class="col-3 d-flex align-self-center"><?php 
						if(isset($_POST["full_name"])){
							
							echo $_POST["full_name"];
						}
					?></div>
				</div>
				
			</div>
			<div class="row align-items-center mt-3">
				<div class="d-flex justify-content-center gap-3">
					<div class="col-1"><label class="input-title custom-border" >Giới tính</label></div>
					<div class="col-3 d-flex align-self-center">
					<?php
						if(isset($_POST["gender"])){
							$genders = array("Nam", "Nữ");
							echo $genders[$_POST["gender"]];
						}
						?>
					</div>
					
				</div>
			</div>
			
			<div class="row align-items-center mt-3">
				<div class="d-flex justify-content-center gap-3">
					<div class="col-1"><label class="input-title custom-border" >Ngày sinh</label></div>
					<div  class="col-3 d-flex align-self-center">
						<?php
							if(isset($_POST["birthday"])){
								echo $_POST["birthday"];
							}
						?>
					</div>
				</div>
			</div>
			<div class="row mt-3 h-fit-content">
				<div class="d-flex justify-content-center gap-3">
					<div class="col-1"><label class="input-title custom-border">Địa chỉ</label></div>
					<div  class="col-3 d-flex align-self-center">
						<?php
							if(isset($_POST["address"])){
								echo $_POST["address"];
							}
						?>
					</div>
				</div>
			</div>
			<div class="row mt-3 h-fit-content">
				<div class="d-flex justify-content-center gap-3">
					<div class="col-1"><label class="input-title custom-border">Thông tin khác</label></div>
					<div  class="col-3">
						<?php
							if(isset($_POST["info"])){
								
								echo $_POST["info"];
							}
						?>
					</div>
				</div>
			</div>
			<div class="row mt-3 d-none">
				<div class="d-flex justify-content-center">
					<input class="btn btn-register" type="submit" value="Xác nhận" />
				</div>
			</div>
		</div>
	</form>
	
	
	<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.7.1/jquery.min.js" integrity="sha512-v2CJ7UaYy4JwqLDIrZUI/4hqeoQieOmAZNXBeQyjo21dadnwR+8ZaIJVT8EE2iyI61OV8e6M8PP2/4hpQINQ/g==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
	<script type="text/javascript">
		
	</script>
</body>
</html>