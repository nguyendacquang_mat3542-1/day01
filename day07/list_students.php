<html>
<head>
    <meta charset="UTF-8">
	<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
	<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous"></script>
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.10.0/css/bootstrap-datepicker.min.css" integrity="sha512-34s5cpvaNG3BknEWSuOncX28vz97bRI59UnVtEEpFX536A7BtZSJHsDyFoCl8S7Dt2TPzcrCEoHBGeM4SUBDBw==" crossorigin="anonymous" referrerpolicy="no-referrer" />
</head>
<style>
	body {
	   margin:40px 0px;
	   padding: 0px;
	}
	
	
	
</style>
<body>
	<div class="container">
		<div class="row justify-content-md-center my-3">
			<div class="col col-lg-1">
				<span>Khoa</span>
			</div>
			
			<div class="col col-lg-2">
				<select id="select-major" class="form-select">
					<?php
						$majors = array(""=>"--Chọn phân khoa--" ,"MAT"=>"Khoa học máy tính", "KDL"=>"Khoa học vật liệu");
						
						foreach($majors as $value => $text) {
							echo "<option value='$value'>$text</option>";
						}
					?>
				</select>
			</div>
			</div>
		<div class="row justify-content-md-center">
			<span class="col col-lg-1">Từ khoá</span>
			<div class="col col-lg-2">
				<input id="input-keyword" class="w-100"/>
			</div>
		</div>
		<div class="row justify-content-md-center my-4">
			<div class="col col-md-auto"><button id="btn-search" class="btn btn-info">Tìm kiếm</button</div>
		</div>
	</div>
	<div class="d-flex">
		<div id="total-record" class="flex-grow-1">
			a
		</div>
		<div class="p-2">
			<a class="btn btn-secondary" href="register.php">Thêm</a>
		</div>
	</div>
	<table class="table">
  <thead>
    <tr>
      <th scope="col">No</th>
      <th scope="col">Tên sinh viên</th>
      <th scope="col">Khoa</th>
      <th scope="col">Action</th>
    </tr>
  </thead>
  <tbody id="rows-view">
  </tbody>
</table>
	
	
	<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.7.1/jquery.min.js" integrity="sha512-v2CJ7UaYy4JwqLDIrZUI/4hqeoQieOmAZNXBeQyjo21dadnwR+8ZaIJVT8EE2iyI61OV8e6M8PP2/4hpQINQ/g==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
	<script type="text/javascript">
		$(document).ready(() => {
			init()
			$( "#btn-search" ).trigger( "click" );
		})
		
		function init(){
			$('#btn-search').click(() =>{
				$('#rows-view').children().remove()
				$('#total-record').html('')
				const keyword = $('#input-keyword').val()
				const major = $('#select-major').val()
				
				$.get( "search_students.php", { keyword: keyword, major: major } )
				  .done(function(r) {
					//alert( "Data Loaded: " + r );
					const resp = JSON.parse(r);
					
					if(resp.success){
						resp.data.forEach((e, i) => {
							var row = `<tr>
										  <th scope="row">${i+1}</th>
										  <td>${e.full_name}</td>
										  <td>${e.major}</td>
										  <td>
											<button type="button" class="btn btn-danger">Xoá</button>
											<button type="button" class="btn btn-primary">Sửa</button>
										  </td>
										</tr>`;
							$('#rows-view').append(row)
						})
						
						$('#total-record').html(`Số sinh viên đã tìm thấy: ${resp.data.length}`)
					}
				  });
			})
		}
	</script>
</body>
</html>